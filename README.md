# Photo Filter App

Ce projet est une application iOS développée en Swift qui permet de prendre une photo, d'appliquer un filtre et de l'enregistrer dans la galerie.

## Fonctionnalités

L'application propose les fonctionnalités suivantes :

- Prise de photo : l'utilisateur peut prendre une photo à partir de l'application.
- Application de filtre : l'utilisateur peut appliquer un filtre à la photo sélectionnée.
- Enregistrement de la photo : l'utilisateur peut enregistrer la photo avec le filtre appliqué dans la galerie de son téléphone.


## Architecture

L'application suit une architecture MVC (Model-View-Controller). Le modèle contient les données de l'application, la vue gère l'affichage des données et le contrôleur gère les interactions entre le modèle et la vue.

## Bibliothèques utilisées

L'application utilise la bibliothèque Core Image pour l'application de filtres aux photos.

## Prérequis

- Xcode 12 ou une version ultérieure
- iOS 14.0 ou une version ultérieur

## Installation

1. Clonez ce dépôt sur votre ordinateur.
2. Ouvrez le fichier `swift_project.xcodeproj` avec Xcode.
3. Compilez et exécutez l'application dans le simulateur ou sur votre appareil iOS.